module.exports.run = async (bot, message, args) => {
message.delete()
let Rank;
if(message.author.id == "255395378885689344" || message.author.id == "208688963936845824" || message.author.id == "189828644019896321" || message.author.id == "489003898783399937" || message.author.id == "267442453542469633") {
Rank = "Management";
} else {
Rank = "Staff";
}
let ID = message.channel.topic.slice(0, 19).trim();
bot.fetchUser(ID).then((mailer) => {
mailer.send(`**(${Rank}):** ${args.join(" ")}`);
message.channel.send(`**(${message.author.tag}):** ${args.join(" ")}`);
});
}
module.exports.command = {
    name: 'reply',
    aliases: ["r"],
    usable: "Staff",
    description: "Replies to the user contacting in a ModMail thread.",
    usage: ">close",
    category: "ModMail",
    enabled: true
};