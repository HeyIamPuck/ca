const Discord = require("discord.js");
module.exports.run = async (bot, message, args) => {
if(message.member.roles.has("431903134592532482")){
    try {
if(message.channel.topic.includes("Chino's ModMail")) {
if(!args[0]){
message.reply("Thread scheduled to close. Closing this thread will delete the channel permanently, are you sure the case is over with and the recipient received their support? If so please confirm it by doing `>close confirm`");
} else if(args[0] == "confirm"){
let userd = message.channel.topic.slice(0,19).trim();
let management = bot.channels.get("472388862107189268");
management.send(`<@${message.author.id}> (ID: ${message.author.id}) closed <@${userd}> (ID: ${userd})'s ModMail thread.`)
message.channel.delete()
} else {
return message.reply("Please only use this command in modmail threads");
}
}
    } catch(err) {
        console.log(err);
    }
} else {
return;
}
}
module.exports.command = {
    name: 'close',
    aliases: ["endthread"],
    usable: "Staff",
    description: "Closes the ModMail thread.",
    usage: ">close",
    category: "ModMail",
    enabled: true
};