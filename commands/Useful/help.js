const Discord = require('discord.js')
module.exports.run = async (bot, message, args) => {
    let input = args[0];
    if (!input) {
        imgcats = bot.commands.map(x => x.command).filter(x => x.category === 'Image')
        dbcats = bot.commands.map(x => x.command).filter(x => x.category === 'Economy')
        imgmcats = bot.commands.map(x => x.command).filter(x => x.category === 'Image Manipulation')
        misccats = bot.commands.map(x => x.command).filter(x => x.category === 'Misc')
        modcats = bot.commands.map(x => x.command).filter(x => x.category === 'Moderator')
        mmcats = bot.commands.map(x => x.command).filter(x => x.category === 'ModMail')
        owncats = bot.commands.map(x => x.command).filter(x => x.category === 'Owner')
        usefulcats = bot.commands.map(x => x.command).filter(x => x.category === 'Useful')
        let imghelp = []
        let dbhelp = []
        let imagemanhelp = []
        let mischelp = []
        let modhelp = []
        let ownhelp = []
        let usefulhelp = []
        let mmhelp = []
        for (i = 0; i < imgcats.length; i++) {
            imghelp.push(`➤**${imgcats[i].name}** - *${imgcats[i].description}*`)
        }
        for (i = 0; i < dbcats.length; i++) {
            dbhelp.push(`➤**${dbcats[i].name}** - *${dbcats[i].description}*`)
        }
        for (i = 0; i < imgmcats.length; i++) {
            imagemanhelp.push(`➤**${imgmcats[i].name}** - *${imgmcats[i].description}*`)
        }
        for (i = 0; i < misccats.length; i++) {
            mischelp.push(`➤**${misccats[i].name}** - *${misccats[i].description}*`)
        }
        for (i = 0; i < modcats.length; i++) {
            modhelp.push(`➤**${imgcats[i].name}** - *${imgcats[i].description}*`)
        }
        for (i = 0; i < mmcats.length; i++) {
            mmhelp.push(`➤**${mmcats[i].name}** - *${mmcats[i].description}*`)
        }
        for (i = 0; i < owncats.length; i++) {
            ownhelp.push(`➤**${owncats[i].name}** - *${owncats[i].description}*`)
        }
        for (i = 0; i < usefulcats.length; i++) {
            usefulhelp.push(`➤**${usefulcats[i].name}** - *${usefulcats[i].description}*`)
        }
        const helphelp = new Discord.RichEmbed()
            .setAuthor("Categories Help!", message.author.displayAvatarURL)
            .setColor("0xFFB6C1")
            .setDescription(`**Hey cutie! You can view commands in a category by clicking the reaction of that category.**\n\n(${imghelp.length}) <:Image:526076454749077527> **Image** - Images and cuteness.\n(${imagemanhelp.length}) <:Manipulation:526076189446766603> **Image Manipulation**  - Images with your own twist.\n(${usefulhelp.length}) <:Useful:526075222278012928> **Useful** - Helpful and cool.\n(${dbhelp.length}) <:Economy:526076349937352727> **Economy** - Beans and more.\n(${mischelp.length}) <:Misc:527896963446996992> **Misc** - Random stuff.`)
            .setFooter("You can also check commands by doing >help <command>")
            .setThumbnail(bot.user.displayAvatarURL)
        const imageembed = new Discord.RichEmbed()
            .setDescription(imghelp)
            .setColor('0xF0A3A3')
            .setFooter("You can also check commands by doing >help <command>")
            .setAuthor('Image help', bot.user.displayAvatarURL)
            .setThumbnail('https://cdn.discordapp.com/emojis/526076454749077527.png?v=1')
        const imagemanipembed = new Discord.RichEmbed()
            .setDescription(imagemanhelp)
            .setColor('0xF0A3A3')
            .setFooter("You can also check commands by doing >help <command>")
            .setAuthor('Image manipulation help', bot.user.displayAvatarURL)
            .setThumbnail('https://cdn.discordapp.com/emojis/526076189446766603.png?v=1')
        const dbembed = new Discord.RichEmbed()
            .setDescription(dbhelp)
            .setColor('0xF0A3A3')
            .setFooter("You can also check commands by doing >help <command>")
            .setAuthor('Economy help', bot.user.displayAvatarURL)
            .setThumbnail('https://cdn.discordapp.com/emojis/526076349937352727.png?v=1')
        const miscembed = new Discord.RichEmbed()
            .setDescription(mischelp)
            .setColor('0xF0A3A3')
            .setFooter("You can also check commands by doing >help <command>")
            .setAuthor('Misc help', bot.user.displayAvatarURL)
            .setThumbnail('https://cdn.discordapp.com/emojis/527896963446996992.png?v=1')
        const modembed = new Discord.RichEmbed()
            .setTitle('Moderator help')
            .setDescription(modhelp)
            .setColor('0xF0A3A3')
            .setFooter("You can also check commands by doing >help <command>")
        const mmembed = new Discord.RichEmbed()
            .setTitle('Modmail help')
            .setDescription(mmhelp)
            .setColor('0xF0A3A3')
            .setFooter("You can also check commands by doing >help <command>")
        const usefulembed = new Discord.RichEmbed()
            .setDescription(usefulhelp)
            .setColor('0xF0A3A3')
            .setFooter("You can also check commands by doing >help <command>")
            .setAuthor('Useful commands help', bot.user.displayAvatarURL)
            .setThumbnail('https://cdn.discordapp.com/emojis/526075222278012928.png?v=1')
        const ownerembed = new Discord.RichEmbed()
            .setTitle('Owner help')
            .setDescription(ownhelp)
            .setColor('0xF0A3A3')
            .setFooter("You can also check commands by doing >help <command>")

        if (message.author.id === '208688963936845824' || message.author.id === '233667448887312385') pages = [imageembed, imagemanipembed, usefulembed, dbembed, miscembed, modembed, mmembed, ownerembed]
        else pages = [imageembed, imagemanipembed, usefulembed, dbembed, miscembed]
        control = 0
        pageembed = await message.channel.send(helphelp)
        imagereaction = await pageembed.react('526076454749077527')
        manipreaction = await pageembed.react('526076189446766603')
        usefulreaction = await pageembed.react('526075222278012928')
        economyreaction = await pageembed.react('526076349937352727')
        miscreaction = await pageembed.react('527896963446996992')
        stop = await pageembed.react('527963498538336256')
        info = await pageembed.react('ℹ')
        usr = message.author.id
        const filter = (reaction, user) => user.id === message.author.id
        let amt = 1
        const collector = pageembed.createReactionCollector(filter);
        collector.on('collect', async r => {
            if (r.emoji.id === '527896963446996992') {
                pageembed.edit(miscembed)
                await miscreaction.remove(usr)
            } else if (r.emoji.id === '526076189446766603') {
                pageembed.edit(imagemanipembed)
                await manipreaction.remove(usr)
            } else if (r.emoji.id === '526076454749077527') {
                pageembed.edit(imageembed)
                await manipreaction.remove(usr)
            } else if (r.emoji.id === '526075222278012928') {
                pageembed.edit(usefulembed)
                await usefulreaction.remove(usr)
            } else if (r.emoji.id === '526076349937352727') {
                pageembed.edit(dbembed)
                await economyreaction.remove(usr)
            } else if (r.emoji.name === 'ℹ') {
                await info.remove(usr)
                control = 1
            } else if (r.emoji.id === '527963498538336256') {
                control = 2
                pageembed.delete()
                message.delete()
                collector.stop()
            }
            if (control == 1) {
                pageembed.edit(helphelp)
            } else {
                control = 0
            }
        });
    } else if (bot.commands.has(input)) {
        var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        var event = new Date();
        let cmd = bot.commands.get(input).command;
        if (!input) {
            // Send the first help embed that has the categories.
        } else if (bot.commands.has(input)) {
            let embed = new Discord.RichEmbed()
                .setAuthor(cmd.name.toUpperCase())
                .setColor("0xFFB6C1")
                .setDescription(`**➤Description:**\n ${cmd.description}\n**➤Usage:**\n ${cmd.usage}\n**➤Useable By:**\n ${cmd.usable}\n**➤Aliases:**\n ${cmd.aliases}\n**➤Enabled:**\n ${cmd.enabled}`)
                .setThumbnail(bot.user.displayAvatarURL)
                .setFooter(event.toLocaleDateString('eng-GB', options))
            message.channel.send(embed);
        } else {
            return message.channel.send(`Command not found`)
        }
    } else {
        return message.channel.send(`Command/category not found`)
    }
}
module.exports.command = {
    name: 'help',
    aliases: [""],
    permission: "",
    description: "The help command, where you can view information about all other commands.",
    usage: ">help",
    category: "Useful",
    enabled: true
};