const Discord = require("discord.js")
module.exports.run = async (bot, message, args) => {
    if (!message.author.id === "208688963936845824" && !message.author.id === "233667448887312385" && !message.author.id === "255395378885689344") return;
    if (args[0] === "on") {
        message.channel.send('Locked server.')
        let channelz = message.guild.channels;
        channelz.forEach((channel) => {
            channel.overwritePermissions(message.guild.id, {
                SEND_MESSAGES: false
            })
        });
    } else if(args[0] === "off") {
        let channelc = ["300647455953780736", "513079879688912919", "325359694233206785", "513080063319605249", "333253237752856579", "513698321659985920", "513079980079448065", "513080029119250432", "513080336427515911", "513085526316679177", "520723571815874585"]
        message.channel.send('Unlocked server.')
        channelc.forEach((item) => {
            unlchannel = message.channels.find(item)
            item.overwritePermissions(message.guild.id, {
                SEND_MESSAGES: true
            })
        });
    } else {
        message.channel.send("Invalid toggle.")
    }
}
module.exports.command = {
    name: 'lockdown',
    aliases: ["lock"],
    permission: "",
    description: "Locks or unlocks the server.",
    usage: ">lockdown [on|off]",
    category: "Moderator",
    enabled: true
}