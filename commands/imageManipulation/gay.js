const Discord = require("discord.js")

module.exports.run = async (bot,message,args) => {
    const key = require('../../gay/keys.json')
    const superagent = require('superagent')
    let messagea = await message.channel.send('Processing, please wait...');
    let start = Date.now();
    let files = [];
    let botuser = message.mentions.users.first() || message.author;

    if(!botuser) {
        files.push(message.member.avatarURL);
    } else if(botuser && botuser.id == message.member.id) {
        files.push(botuser.avatarURL);
    }

    let value = await superagent
        .post('https://fapi.wrmsr.io/gay')
        .set({
            Authorization: "Bearer f50b00c829a3269a84a03025c414bb1a",
            "Content-Type": "application/json"
        })
        .send({
            "images": files
        })
        .end((err, response) => {
            if (err) return messagea.edit(`An error occurred: ${err.toString()}`);
            else {
                messagea.edit(`That took too long... \`${Date.now() - start}ms\``, { file: response.body, name: `gay.png` })
            };
        });
}
module.exports.command = {
name: 'gay',
aliases: [],
permission: "",
description: "Overlays a gay flag over an image",
usage: ">gay [user mention|user id]",
category: "Images",
enabled: true
}       