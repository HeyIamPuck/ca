const Discord = require("discord.js");
const mysql = require("mysql");
const tokenfile = require('../../gay/keys.json');
const con = mysql.createConnection(tokenfile.mysql);
module.exports.run = async (bot, message, ags) => {
    let target = message.mentions.users.first() || message.author;
    con.query(`SELECT * FROM xp WHERE id = '${target.id}'`, (err, rows) => {
        if (!rows[0]) return message.channel.send("This user has no data yet.");

        let data = rows[0].amount;
        let embed = new Discord.RichEmbed()
            .setAuthor(`${target.username}`, target.displayAvatarURL)
            .setColor("0xFFB6C1")
            .setDescription(`${target.tag} has \`${data}\` coffee beans.`)
            .setThumbnail('http://icons.iconarchive.com/icons/graphicloads/food-drink/256/coffee-bean-icon.png')
            .setFooter("Talk more, get more", bot.user.displayAvatarURL)
        message.channel.send(embed);
    });
}
module.exports.command = {
    name: 'beans',
    aliases: ["balance", "bal"],
    usable: "Users",
    description: "Checks your or someone else's beans balance.",
    usage: ">bal | >bal <@USER/ID>",
    category: "Economy",
    enabled: true
};