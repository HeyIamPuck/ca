const Discord = require("discord.js");
const mysql = require("mysql");
const tokenfile = require('../../gay/keys.json');
const con = mysql.createConnection(tokenfile.mysql);
module.exports.run = async (bot, message, args) => {
        if (message.author.id != "208688963936845824") return;
        let target = message.mentions.users.first() || message.author;
        con.query(`SELECT * FROM xp WHERE id = ${target.id}`, (err, rows) => {
                if (err) console.log(err)
                if (args[0] == "add" || args[0] == "+" && args[1] && args[2]) {
                        let nowxp = rows[0].amount;

                        let addxp = Math.floor(parseInt(nowxp) + parseInt(args[2]));

                        con.query(`UPDATE xp SET amount = ${addxp} WHERE id = '${target.id}'`, (err, rows) => {
                                message.channel.send(`I have updated <@${target.id}>'s balance and added \`${args[2]}\`.`);
                        });
                } else if (args[0] == "remove" || args[0] == "-" && args[1] && args[2]) {
                        let xp = rows[0].amount;

                        let newxp = Math.floor(parseInt(xp) - parseInt(args[2]));

                        con.query(`UPDATE xp SET amount = ${newxp} WHERE id = '${target.id}'`, (err, rows) => {
                                if (err) console.log(err)
                                message.channel.send(`I have updated <@${target.id}>'s balance and removed \`${args[2]}\`.`);
                        });
                } else if (args[0] == "zero" || args[0] == "wipe" && args[1]) {
                        con.query(`UPDATE xp SET amount = 0 WHERE id = '${target.id}'`, (err, rows) => {
                                if (err) console.log(err)
                                message.channel.send(`I have wiped <@${target.id}>'s balance.`)
                        });
                } else if (!args[0]) {
                        message.reply(`⛔ **Wrong format**\n**Add:** \`>economy + <@USER/ID> <amount>\`\n**Remove:** \`>economy - <@USER/ID> <amount>\`\n**wipe:** \`>economy wipe <@USER/ID>\``)
                } else {
                        return;
                }
        });
}
module.exports.command = {
    name: 'economy',
    aliases: ["eco"],
    usable: "Joe",
    description: "Changes users balance.",
    usage: ">eco + <@USER/ID> <AMOUNT> | >eco - <@USER/ID> <AMOUNT> | >eco wipe <@USER/ID>",
    category: "Economy",
    enabled: true
};
