const Discord = require("discord.js");
const mysql = require("mysql");
const tokenfile = require('../../gay/keys.json');
const con = mysql.createConnection(tokenfile.mysql);
module.exports.run = async (bot, message, ags) => {
    let target = message.mentions.users.first() || message.author;
    con.query(`SELECT * FROM xp ORDER BY amount DESC LIMIT 20`, (err, rows) => {
        let index = 0;
        let embed = new Discord.RichEmbed()
            .setAuthor("Leaderboard", message.guild.iconURL)
            .setDescription(rows.map(row => `**${++index}** -  **${!bot.users.get(row.id) ? row.id : bot.users.get(row.id)}** with **${row.amount}** coffee beans.`))
            .setThumbnail('http://icons.iconarchive.com/icons/graphicloads/food-drink/256/coffee-bean-icon.png')
            .setColor("0xFFB6C1")
        message.channel.send(embed)
    });
}
module.exports.command = {
    name: 'leaderboard',
    aliases: ["top", "topbeans"],
    usable: "Users",
    description: "Check the top 20 users with the most beans.",
    usage: ">top",
    category: "Economy",
    enabled: true
};