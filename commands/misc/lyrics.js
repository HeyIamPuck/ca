const Discord = require("discord.js");
const {
    get
} = require("snekfetch");
module.exports.run = async (bot, message, args) => {
    try {
        let link = "https://some-random-api.ml/lyrics?title="
        let name = args.join("℅");
        let finalLink = link + name
        get(finalLink).then(res => {
            if (res.body.error) {
                message.channel.send('Sorry, couldn\'t find that song.');
            } else {
                const embed = new Discord.RichEmbed()
                    .setDescription(`Only showing the first 1000 characters\n**Lyrics:**\n\n${res.body.lyrics.slice(0, 1000)}`)
                    .setColor("0xFFB6C1")
                    .setAuthor(res.body.author)
                    .setTitle(res.body.title)
                    .setThumbnail(res.body.thumbnail.genius)
                setTimeout(() => {
                    return message.channel.send(embed);
                }, 300);
            };
        })
    } catch (err) {
        console.log(err);
    }
}

module.exports.command = {
    name: 'lyrics',
    aliases: ["song"],
    usable: "Users",
    description: "Sends out 1000 characters of lyrics of the desired song.",
    usage: ">lyrics <SONG>",
    category: "Misc",
    enabled: true
};