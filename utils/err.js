const Discord = require('discord.js')
const ids = require('../utils/ids.json')
function errorOut(err) {
    console.log(err)
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    var event = new Date();
    let embed = new Discord.RichEmbed()
    .setAuthor('An error has occurred',`http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/sign-error-icon.png`)
    .setColor(colors.error)
    .setThumbnail(bot.user.displayAvatarURL)
    .setDescription(err)
    .setFooter(event.toLocaleDateString('eng-GB', options))
    bot.channels.get(ids.errorchannel).send(embed)
    return;
}